## Wikimedia Input Tools - Firefox Add-on

### Features

- Enables tying in more than 220 input methods across more than 200 languages which [jquery.ime](https://github.com/wikimedia/jquery.ime) supports.

Contributions are welcome!

### License: GPLv3
